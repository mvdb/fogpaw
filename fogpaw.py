from gpaw import restart
from gpaw.utilities.ps2ae import PS2AE
from ase.io import write
from ase.units import Bohr
from ase.parallel import parprint
import numpy as np
import sys

class FOGPAW():
    '''    
    Written with help from the following GPAW tutorials:
    http://wiki.fysik.dtu.dk/gpaw/tutorials/plotting/plot_wave_functions.html
    http://wiki.fysik.dtu.dk/gpaw/tutorials/ps2ae/ps2ae.html

    Arguments:
    gpwname: path to GPAW restart file 
             (needs to include the wave functions).
    ae: whether to add the PAW corrections to the pseudo wfs.
    fod_cube_file: filename to which the FOD will be written in cube format.
                   Default is None, which is not to write it.
    '''
    def __init__(self, gpwname, ae=True, fod_cube_file=None):
        self.gpwname = gpwname
        self.ae = ae
        self.fod_cube_file = fod_cube_file

    def run(self, gpawtxt=None):
        # initialize
        parprint('Starting FOGPAW run')
        atoms, calc = restart(self.gpwname, txt=gpawtxt)
        nbands = calc.get_number_of_bands()
        kpts = calc.get_ibz_k_points()
        kpt_weights = calc.get_k_point_weights()
        e_fermi = calc.get_fermi_level()
        spin_polarized = calc.get_spin_polarized()
        nspins = 2 if spin_polarized else 1

        if self.ae:
            transformer = PS2AE(calc, h=0.1)
            shape = np.shape(transformer.get_wave_function(0))
        else:
            shape = np.shape(calc.get_pseudo_wave_function())

        fod = np.zeros(shape)
        voxel = atoms.get_volume()*(Bohr**3)/np.prod(shape)

        # compute FOD and N_FOD
        parprint('Band\tOccup.\tEigval.\tIntRho')
        parprint('-------------------------------')
        for ikpt,kpt in enumerate(kpts):
            for spin in range(nspins):
                parprint('kpt: {0}, spin: {1}'.format(kpt, spin))
                parprint('-------------------------------')

                eigs = calc.get_eigenvalues(kpt=ikpt, spin=spin)
                occup = calc.get_occupation_numbers(kpt=ikpt, spin=spin)
                for band in range(nbands):
                    if self.ae:
                        wf = transformer.get_wave_function(band, k=ikpt, s=spin)
                        wf /= np.sqrt(Bohr**3)
                    else:
                        wf = calc.get_pseudo_wave_function(band=band, 
                                                           kpt=ikpt, 
                                                           spin=spin)

                    f = occup[band]/kpt_weights[ikpt]
                    e = eigs[band]
                    # wf is in units of 1/Bohr^(3/2)     
                    dens = np.real(wf*np.conj(wf))
                    int_rho = np.sum(dens)*voxel
                    # for AE, int_rho is very close to
                    # transformer.gd.integrate(dens)
                    parprint('%0.4d\t%.3f\t%.3f\t%.3f' % (band, f, e, int_rho))

                    if spin_polarized:
                        d1,d2 = (1,1) if e < e_fermi[spin] else (0,-1)
                    else:
                        d1,d2 = (2,1) if e < e_fermi[spin] else (0,-1)

                    fod += (d1 - d2*f)*dens*kpt_weights[ikpt]

                parprint('-------------------------------')

        nfod = np.sum(fod)*voxel
        parprint('NFOD:', nfod)
 
        if self.fod_cube_file is not None:
            write(self.fod_cube_file, atoms, format='cube', data=fod)

        return

if __name__=='__main__':
    gpwname = sys.argv[1]
    fogpaw = FOGPAW(gpwname)
    fogpaw.run()
