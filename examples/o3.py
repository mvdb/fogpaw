''' Test with ozone molecule (TPSS functional) '''
import os
from ase.units import kB
from ase.build import molecule
from gpaw import GPAW, FermiDirac, Mixer
from fogpaw import FOGPAW

def run_gpaw(prefix):
    T = 5000.
    width = kB*T
    calc = GPAW(mode='fd', 
                basis='dzp',
                h=0.20,
                xc='PBE',
                kpts=(1,1,1),
                spinpol=False,
                eigensolver='rmm-diis',
                occupations=FermiDirac(width, fixmagmom=True),
                mixer=Mixer(beta=0.2, nmaxold=5, weight=50.0),
                txt=prefix+'.txt',
                )

    m = molecule('O3')
    m.center(vacuum=5)
    m.set_pbc([True]*3)
    m.set_initial_magnetic_moments([0.]*len(m))

    calc.initialize(m)
    calc.get_potential_energy()
    calc.set(xc='TPSS')
    calc.get_potential_energy()
    calc.write(prefix+'.gpw', mode='all') 
    return

if __name__=='__main__':
    prefix = 'o3_tpss'
    gpwname = prefix + '.gpw'
    if not os.path.exists(gpwname):
        run_gpaw(prefix)
    fogpaw = FOGPAW(gpwname, fod_cube_file=None)
    fogpaw.run()
