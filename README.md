FOGPAW
===========================
Code and examples to perform Fractional Orbital (FO) analysis
in GPAW, following the work of Grimme and Hansen [1]. FO density (FOD)
plots and FO numbers (NFOD) can be used to visualize and quantify
static electron correlation.

[1] S. Grimme and A. Hansen, http://dx.doi.org/10.1002/anie.201501887

Note: FOGPAW can also be run using the gpaw-python interpreter
(either serially or in parallel), but currently the code takes no
advantage of parallelization (every process is doing the same thing).